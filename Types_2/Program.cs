﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Types_2
{
    class Program
    {
        static void Main(string[] args)
        {
            //declaring a value type
            int value_type_example_variable;
            //declaring a refrence type
            string reference_type_example_variable;

            //assigning values to both.

            value_type_example_variable = 10;
            reference_type_example_variable = "hello";

            //showing their contents
            Console.WriteLine("value_type_example_variable contains - " + value_type_example_variable);
            Console.WriteLine("reference_type_example_variable contains - " + reference_type_example_variable);

            //I am putting this so that I can stop the console from vanishing
            Console.ReadLine();

            //note, here, you wont see any difference about the two types. At this point you simply need to understand that 
            //int is a value type and hence it actually stores the value i.e. 10 in its stack memory
            //string is a reference type and hence it only stores a 'reference' to the actual string that is stored on the heap memory


        }

    }
}
